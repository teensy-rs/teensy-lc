pub use super::hal::digital::v2::OutputPin as _embedded_hal_digital_v2_OutputPin;
pub use super::hal::prelude::*;

pub use super::fgpio::FgpioExt as _teensy_lc_hal_fgpio_FgpioExt;
pub use super::mcg::McgExt as _teensy_lc_hal_mcg_McgExt;
