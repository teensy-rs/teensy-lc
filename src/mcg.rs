use core::marker::PhantomData;

use cortex_m::asm::nop;

use crate::dev::{osc0, sim, MCG};

pub trait McgExt {
    type Constrained;

    fn constrain(self) -> Self::Constrained;
}

// typestate for clock states (table 24.18, fig. 24-16)

pub struct FEI; // FLL Engaged Internal, initial mode; <-> {FEE, FBE, FBI}
pub struct FEE; // FLL Engaged External <-> FEI, FBI, FBE
pub struct FBI; // FLL Bypassed Internal <-> FEI, FEE, FBE, BLPI
pub struct FBE; // FLL Bypassed External <-> FEI, FEE, FBI, BLPE, PBE
pub struct PBE; // PLL Bypassed External <-> FBE, BLPE, PEE
pub struct PEE; // PLL Engaged External <-> PBE
pub struct BLPI; // Bypassed Low Power Internal <-> FBI
pub struct BLPE; // Bypassed Low Power External <-> FBE, PBE

pub struct Mcg<MODE> {
    mcg: MCG,
    _mode: PhantomData<MODE>,
}

impl McgExt for MCG {
    type Constrained = Mcg<FEI>;

    fn constrain(self) -> Self::Constrained {
        Self::Constrained {
            mcg: self,
            _mode: PhantomData,
        }
    }
}

fn set_clkdiv1(sim: &sim::RegisterBlock, outdiv1: u8, outdiv4: u8) {
    sim.clkdiv1
        .write(|w| w.outdiv1().bits(outdiv1).outdiv4().bits(outdiv4));
}

fn init_osc0(osc0: &osc0::RegisterBlock) {
    osc0.cr
        .write(|w| w.sc8p().set_bit().sc2p().set_bit().erclken().set_bit());
}

// Implement methods on initial state of the clock
impl Mcg<FEI> {
    fn into_fbi(self) -> Mcg<FBI> {
        self.into()
    }

    fn into_blpi(self) -> Mcg<BLPI> {
        self.into_fbi().into()
    }

    fn into_fbe(self) -> Mcg<FBE> {
        self.into()
    }

    fn into_blpe(self) -> Mcg<BLPE> {
        self.into_fbe().into()
    }

    fn into_pbe(self) -> Mcg<PBE> {
        self.into_fbe().into()
    }

    /// 48 MHz core, 48 MHz bus, 24 MHz flash, USB = 96 / 2
    pub fn init_clock_48mhz(
        self,
        sim: &sim::RegisterBlock,
        osc0: &osc0::RegisterBlock,
    ) -> Mcg<PEE> {
        init_osc0(osc0);
        let mcg0 = self.into_pbe();

        // SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(1) | SIM_CLKDIV1_OUTDIV4(1);
        set_clkdiv1(sim, 1, 1);

        let mcg = mcg0.into_pee();

        // SIM_SOPT2 = SIM_SOPT2_USBSRC | SIM_SOPT2_PLLFLLSEL | SIM_SOPT2_CLKOUTSEL(6)
        //	| SIM_SOPT2_UART0SRC(1) | SIM_SOPT2_TPMSRC(1);
        sim.sopt2.write(|w| {
            w.usbsrc()
                .set_bit()
                .pllfllsel()
                .set_bit()
                .clkoutsel()
                ._110()
                .uart0src()
                .bits(1)
                .tpmsrc()
                .bits(1)
        });

        mcg
    }

    /// 24 MHz core, 24 MHz bus, 24 MHz flash, USB = 96 / 2
    pub fn init_clock_24mhz(
        self,
        sim: &sim::RegisterBlock,
        osc0: &osc0::RegisterBlock,
    ) -> Mcg<PEE> {
        init_osc0(osc0);
        let mcg0 = self.into_pbe();

        // SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(3) | SIM_CLKDIV1_OUTDIV4(0);
        set_clkdiv1(sim, 3, 0);

        let mcg = mcg0.into_pee();

        // SIM_SOPT2 = SIM_SOPT2_USBSRC | SIM_SOPT2_PLLFLLSEL | SIM_SOPT2_CLKOUTSEL(6)
        //	| SIM_SOPT2_UART0SRC(1) | SIM_SOPT2_TPMSRC(1);
        sim.sopt2.write(|w| {
            w.usbsrc()
                .set_bit()
                .pllfllsel()
                .set_bit()
                .clkoutsel()
                ._110()
                .uart0src()
                .bits(1)
                .tpmsrc()
                .bits(1)
        });

        mcg
    }

    /// 16 MHz core, 16 MHz bus, 16 MHz flash
    pub fn init_clock_16mhz(
        self,
        sim: &sim::RegisterBlock,
        osc0: &osc0::RegisterBlock,
    ) -> Mcg<BLPE> {
        init_osc0(osc0);
        let mcg = self.into_blpe();

        // SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(0) | SIM_CLKDIV1_OUTDIV4(0);
        set_clkdiv1(sim, 0, 0);

        // SIM_SOPT2 = SIM_SOPT2_TRACECLKSEL | SIM_SOPT2_CLKOUTSEL(6) | SIM_SOPT2_UART0SRC(2);
        // FIXME:TRACECLKSEL
        sim.sopt2.write(|w| w.clkoutsel()._110().uart0src().bits(2));

        mcg
    }

    /// 8 MHz core, 8 MHz bus, 8 MHz flash
    pub fn init_clock_8mhz(
        self,
        sim: &sim::RegisterBlock,
        osc0: &osc0::RegisterBlock,
    ) -> Mcg<BLPE> {
        init_osc0(osc0);
        let mcg = self.into_fbe().into_blpe();

        // config divisors: 8 MHz core, 8 MHz bus, 8 MHz flash
        // SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(1) | SIM_CLKDIV1_OUTDIV4(0);
        set_clkdiv1(sim, 1, 0);

        // SIM_SOPT2 = SIM_SOPT2_TRACECLKSEL | SIM_SOPT2_CLKOUTSEL(6) | SIM_SOPT2_UART0SRC(2);
        // FIXME:TRACECLKSEL
        sim.sopt2.write(|w| w.clkoutsel()._110().uart0src().bits(2));

        mcg
    }

    /// 4 MHz core, 4 MHz bus, 2 MHz flash
    pub fn init_clock_4mhz(
        self,
        sim: &sim::RegisterBlock,
        osc0: &osc0::RegisterBlock,
    ) -> Mcg<BLPE> {
        init_osc0(osc0);
        let mcg = self.into_blpe();

        // since we are running from external clock 16MHz
        // fix outdiv too -> cpu 16/4, bus 16/4, flash 16/4
        // here we can go into vlpr?
        // config divisors: 4 MHz core, 4 MHz bus, 4 MHz flash
        // SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(3) | SIM_CLKDIV1_OUTDIV4(0);
        set_clkdiv1(sim, 3, 0);

        // SIM_SOPT2 = SIM_SOPT2_TRACECLKSEL | SIM_SOPT2_CLKOUTSEL(6) | SIM_SOPT2_UART0SRC(2);
        // FIXME:TRACECLKSEL
        sim.sopt2.write(|w| w.clkoutsel()._110().uart0src().bits(2));

        mcg
    }

    /// 2 MHz core, 1 MHz bus, 1 MHz flash
    pub fn init_clock_2mhz(self, sim: &sim::RegisterBlock) -> Mcg<BLPI> {
        let mcg = self.into_blpi();

        // since we are running from the fast internal reference clock 4MHz
        // but is divided down by 2 so we actually have a 2MHz, MCG_SC[FCDIV] default is 2
        // fix outdiv -> cpu 2/1, bus 2/1, flash 2/2
        // config divisors: 2 MHz core, 2 MHz bus, 1 MHz flash
        // config divisors: 2 MHz core, 1 MHz bus, 1 MHz flash
        // SIM_CLKDIV1 = SIM_CLKDIV1_OUTDIV1(0) | SIM_CLKDIV1_OUTDIV4(1);
        set_clkdiv1(sim, 0, 1);

        // SIM_SOPT2 = SIM_SOPT2_TRACECLKSEL | SIM_SOPT2_CLKOUTSEL(4) | SIM_SOPT2_UART0SRC(3);
        // FIXME:TRACECLKSEL
        sim.sopt2.write(|w| w.clkoutsel()._100().uart0src().bits(3));

        // since we are not going into "stop mode" i removed it
        // SMC_PMCTRL = SMC_PMCTRL_RUNM(2); // VLPR mode :-)
        //smc.pmctrl.write(|w| w.runm()._10());

        mcg
    }
}

impl Mcg<FBI> {
    pub fn into_blpi(self) -> Mcg<BLPI> {
        self.into()
    }
}

impl Mcg<FBE> {
    pub fn into_blpe(self) -> Mcg<BLPE> {
        self.into()
    }

    pub fn into_pbe(self) -> Mcg<PBE> {
        self.into()
    }
}

impl Mcg<PBE> {
    pub fn into_pee(self) -> Mcg<PEE> {
        self.into()
    }
}

// hardware always starts in FEI mode
//  C1[CLKS] bits are written to 00
//  C1[IREFS] bit is written to 1
//  C6[PLLS] bit is written to 0

// teensy docs:
// MCG_SC[FCDIV] defaults to divide by two for internal ref clock
// I tried changing MSG_SC to divide by 1, it didn't work for me

impl Into<Mcg<FBI>> for Mcg<FEI> {
    fn into(self) -> Mcg<FBI> {
        // 01 is written to C1[CLKS]
        // 1 is written to C1[IREFS]
        // Teensy code also sets C1[IRCLKEN]
        self.mcg
            .c1
            .write_with_zero(|w| w.clks()._01().irefs().set_bit().irclken().set_bit());

        // wait for MCGOUT to use oscillator
        // while ((MCG_S & MCG_S_CLKST_MASK) != MCG_S_CLKST(1)) ;
        while self.mcg.s.read().clkst().bits() != 1 {} // TODO: add a NOP?

        // for (n=0; n<10; n++) ; // TODO: why do we get 2 mA extra without this delay?
        for _ in 1..10 {
            nop();
        }

        // MCG_C2 = MCG_C2_IRCS;
        self.mcg.c2.write_with_zero(|w| w.ircs().set_bit());
        // while (!(MCG_S & MCG_S_IRCST)) ;
        while self.mcg.s.read().ircst().bit_is_clear() {} // TODO: add a NOP?

        Mcg {
            mcg: self.mcg,
            _mode: PhantomData,
        }
    }
}

impl Into<Mcg<FBE>> for Mcg<FEI> {
    fn into(self) -> Mcg<FBE> {
        // TODO: ensure crystal is on?

        // enable osc, 8-32 MHz range, low power mode
        // MCG_C2 = MCG_C2_RANGE0(2) | MCG_C2_EREFS;
        self.mcg
            .c2
            .write_with_zero(|w| w.range0()._1x().erefs0().set_bit());

        // switch to crystal as clock source, FLL input = 16 MHz / 512
        // MCG_C1 =  MCG_C1_CLKS(2) | MCG_C1_FRDIV(4);
        self.mcg
            .c1
            .write_with_zero(|w| w.clks().bits(2).frdiv().bits(4));

        // wait for crystal oscillator to begin
        // while ((MCG_S & MCG_S_OSCINIT0) == 0) ;
        while self.mcg.s.read().oscinit0().bit_is_clear() {} // TODO: add a NOP?

        // wait for FLL to use oscillator
        // while ((MCG_S & MCG_S_IREFST) != 0) ;
        while self.mcg.s.read().irefst().bit_is_clear() {} // TODO: add a NOP?

        // wait for MCGOUT to use oscillator
        // while ((MCG_S & MCG_S_CLKST_MASK) != MCG_S_CLKST(2)) ;
        while self.mcg.s.read().clkst().bits() != 2 {} // TODO: add a NOP?

        Mcg {
            mcg: self.mcg,
            _mode: PhantomData,
        }
    }
}
// TODO: impl Into<Mcg<FEE>> for Mcg<FEI> {}
// TODO: impl Into<Mcg<FBI>> for Mcg<FEE> {}
// TODO: impl Into<Mcg<FBE>> for Mcg<FEE> {}
// TODO: impl Into<Mcg<FEI>> for Mcg<FEE> {}
// TODO: impl Into<Mcg<FBE>> for Mcg<FBI> {}
// TODO: impl Into<Mcg<FEE>> for Mcg<FBI> {}
// TODO: impl Into<Mcg<FEI>> for Mcg<FBI> {}

impl Into<Mcg<BLPI>> for Mcg<FBI> {
    fn into(self) -> Mcg<BLPI> {
        //MCG_C2 = MCG_C2_IRCS | MCG_C2_LP;
        self.mcg
            .c2
            .write_with_zero(|w| w.ircs().set_bit().lp().set_bit());

        Mcg {
            mcg: self.mcg,
            _mode: PhantomData,
        }
    }
}

// TODO: impl Into<Mcg<FBI>> for Mcg<FBE> {}
// TODO: impl Into<Mcg<FEE>> for Mcg<FBE> {}
// TODO: impl Into<Mcg<FEI>> for Mcg<FBE> {}

impl Into<Mcg<BLPE>> for Mcg<FBE> {
    fn into(self) -> Mcg<BLPE> {
        // MCG_C2 = MCG_C2_RANGE0(2) | MCG_C2_EREFS | MCG_C2_LP;
        self.mcg.c2.modify(|_, w| w.lp().set_bit());

        Mcg {
            mcg: self.mcg,
            _mode: PhantomData,
        }
    }
}

impl Into<Mcg<PBE>> for Mcg<FBE> {
    fn into(self) -> Mcg<PBE> {
        // This line is not strictly mode change and is Teensy
        // LC-specific:
        // MCG_C5 = MCG_C5_PRDIV0(3); // config PLL input for 16 MHz Crystal / 4 = 4 MHz
        self.mcg.c5.write_with_zero(|w| w.prdiv0()._3());

        // VDIV0 part is Teensy LC-specific (probably)
        // MCG_C6 = MCG_C6_PLLS | MCG_C6_VDIV0(0); // config PLL for 96 MHz output
        self.mcg
            .c6
            .write_with_zero(|w| w.plls().set_bit().vdiv0().bits(0));

        // wait for PLL to start using xtal as its input
        // while (!(MCG_S & MCG_S_PLLST)) ;
        while self.mcg.s.read().pllst().bit_is_clear() {} // TODO: add a NOP?

        // wait for PLL to lock
        // while (!(MCG_S & MCG_S_LOCK0)) ;
        while self.mcg.s.read().lock0().bit_is_clear() {} // TODO: add a NOP?

        Mcg {
            mcg: self.mcg,
            _mode: PhantomData,
        }
    }
}

// TODO: impl Into<Mcg<FBE>> for Mcg<PBE> {}
// TODO: impl Into<Mcg<BLPE>> for Mcg<PBE> {}

impl Into<Mcg<PEE>> for Mcg<PBE> {
    fn into(self) -> Mcg<PEE> {
        // switch to PLL as clock source, FLL input = 16 MHz / 512
        // MCG_C1 = MCG_C1_CLKS(0) | MCG_C1_FRDIV(4);
        self.mcg
            .c1
            .write_with_zero(|w| w.clks().bits(0).frdiv().bits(4));

        // wait for PLL clock to be used
        //while ((MCG_S & MCG_S_CLKST_MASK) != MCG_S_CLKST(3)) ;
        while self.mcg.s.read().clkst().bits() != 3 {} // TODO: add a NOP?

        Mcg {
            mcg: self.mcg,
            _mode: PhantomData,
        }
    }
}

// TODO: impl Into<Mcg<PBE>> for Mcg<PEE> {}
// TODO: impl Into<Mcg<FBI>> for Mcg<BLPI> {}
// TODO: impl Into<Mcg<FBE>> for Mcg<BLPE> {}
// TODO: impl Into<Mcg<PBE>> for Mcg<BLPE> {}
