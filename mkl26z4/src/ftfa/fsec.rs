#[doc = "Reader of register FSEC"]
pub type R = crate::R<u8, super::FSEC>;
#[doc = "Possible values of the field `SEC`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum SEC_A {
    #[doc = "MCU security status is secure."]
    _00,
    #[doc = "MCU security status is secure."]
    _01,
    #[doc = "MCU security status is unsecure. (The standard shipping condition of the flash memory module is unsecure.)"]
    _10,
    #[doc = "MCU security status is secure."]
    _11,
}
impl crate::ToBits<u8> for SEC_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            SEC_A::_00 => 0,
            SEC_A::_01 => 1,
            SEC_A::_10 => 2,
            SEC_A::_11 => 3,
        }
    }
}
#[doc = "Reader of field `SEC`"]
pub type SEC_R = crate::R<u8, SEC_A>;
impl SEC_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> SEC_A {
        match self.bits {
            0 => SEC_A::_00,
            1 => SEC_A::_01,
            2 => SEC_A::_10,
            3 => SEC_A::_11,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_00`"]
    #[inline(always)]
    pub fn is_00(&self) -> bool {
        *self == SEC_A::_00
    }
    #[doc = "Checks if the value of the field is `_01`"]
    #[inline(always)]
    pub fn is_01(&self) -> bool {
        *self == SEC_A::_01
    }
    #[doc = "Checks if the value of the field is `_10`"]
    #[inline(always)]
    pub fn is_10(&self) -> bool {
        *self == SEC_A::_10
    }
    #[doc = "Checks if the value of the field is `_11`"]
    #[inline(always)]
    pub fn is_11(&self) -> bool {
        *self == SEC_A::_11
    }
}
#[doc = "Possible values of the field `FSLACC`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum FSLACC_A {
    #[doc = "Freescale factory access granted"]
    _00,
    #[doc = "Freescale factory access denied"]
    _01,
    #[doc = "Freescale factory access denied"]
    _10,
    #[doc = "Freescale factory access granted"]
    _11,
}
impl crate::ToBits<u8> for FSLACC_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            FSLACC_A::_00 => 0,
            FSLACC_A::_01 => 1,
            FSLACC_A::_10 => 2,
            FSLACC_A::_11 => 3,
        }
    }
}
#[doc = "Reader of field `FSLACC`"]
pub type FSLACC_R = crate::R<u8, FSLACC_A>;
impl FSLACC_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> FSLACC_A {
        match self.bits {
            0 => FSLACC_A::_00,
            1 => FSLACC_A::_01,
            2 => FSLACC_A::_10,
            3 => FSLACC_A::_11,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_00`"]
    #[inline(always)]
    pub fn is_00(&self) -> bool {
        *self == FSLACC_A::_00
    }
    #[doc = "Checks if the value of the field is `_01`"]
    #[inline(always)]
    pub fn is_01(&self) -> bool {
        *self == FSLACC_A::_01
    }
    #[doc = "Checks if the value of the field is `_10`"]
    #[inline(always)]
    pub fn is_10(&self) -> bool {
        *self == FSLACC_A::_10
    }
    #[doc = "Checks if the value of the field is `_11`"]
    #[inline(always)]
    pub fn is_11(&self) -> bool {
        *self == FSLACC_A::_11
    }
}
#[doc = "Possible values of the field `MEEN`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum MEEN_A {
    #[doc = "Mass erase is enabled"]
    _00,
    #[doc = "Mass erase is enabled"]
    _01,
    #[doc = "Mass erase is disabled"]
    _10,
    #[doc = "Mass erase is enabled"]
    _11,
}
impl crate::ToBits<u8> for MEEN_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            MEEN_A::_00 => 0,
            MEEN_A::_01 => 1,
            MEEN_A::_10 => 2,
            MEEN_A::_11 => 3,
        }
    }
}
#[doc = "Reader of field `MEEN`"]
pub type MEEN_R = crate::R<u8, MEEN_A>;
impl MEEN_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> MEEN_A {
        match self.bits {
            0 => MEEN_A::_00,
            1 => MEEN_A::_01,
            2 => MEEN_A::_10,
            3 => MEEN_A::_11,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_00`"]
    #[inline(always)]
    pub fn is_00(&self) -> bool {
        *self == MEEN_A::_00
    }
    #[doc = "Checks if the value of the field is `_01`"]
    #[inline(always)]
    pub fn is_01(&self) -> bool {
        *self == MEEN_A::_01
    }
    #[doc = "Checks if the value of the field is `_10`"]
    #[inline(always)]
    pub fn is_10(&self) -> bool {
        *self == MEEN_A::_10
    }
    #[doc = "Checks if the value of the field is `_11`"]
    #[inline(always)]
    pub fn is_11(&self) -> bool {
        *self == MEEN_A::_11
    }
}
#[doc = "Possible values of the field `KEYEN`"]
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum KEYEN_A {
    #[doc = "Backdoor key access disabled"]
    _00,
    #[doc = "Backdoor key access disabled (preferred KEYEN state to disable backdoor key access)"]
    _01,
    #[doc = "Backdoor key access enabled"]
    _10,
    #[doc = "Backdoor key access disabled"]
    _11,
}
impl crate::ToBits<u8> for KEYEN_A {
    #[inline(always)]
    fn _bits(&self) -> u8 {
        match *self {
            KEYEN_A::_00 => 0,
            KEYEN_A::_01 => 1,
            KEYEN_A::_10 => 2,
            KEYEN_A::_11 => 3,
        }
    }
}
#[doc = "Reader of field `KEYEN`"]
pub type KEYEN_R = crate::R<u8, KEYEN_A>;
impl KEYEN_R {
    #[doc = r"Get enumerated values variant"]
    #[inline(always)]
    pub fn variant(&self) -> KEYEN_A {
        match self.bits {
            0 => KEYEN_A::_00,
            1 => KEYEN_A::_01,
            2 => KEYEN_A::_10,
            3 => KEYEN_A::_11,
            _ => unreachable!(),
        }
    }
    #[doc = "Checks if the value of the field is `_00`"]
    #[inline(always)]
    pub fn is_00(&self) -> bool {
        *self == KEYEN_A::_00
    }
    #[doc = "Checks if the value of the field is `_01`"]
    #[inline(always)]
    pub fn is_01(&self) -> bool {
        *self == KEYEN_A::_01
    }
    #[doc = "Checks if the value of the field is `_10`"]
    #[inline(always)]
    pub fn is_10(&self) -> bool {
        *self == KEYEN_A::_10
    }
    #[doc = "Checks if the value of the field is `_11`"]
    #[inline(always)]
    pub fn is_11(&self) -> bool {
        *self == KEYEN_A::_11
    }
}
impl R {
    #[doc = "Bits 0:1 - Flash Security"]
    #[inline(always)]
    pub fn sec(&self) -> SEC_R {
        SEC_R::new((self.bits & 0x03) as u8)
    }
    #[doc = "Bits 2:3 - Freescale Failure Analysis Access Code"]
    #[inline(always)]
    pub fn fslacc(&self) -> FSLACC_R {
        FSLACC_R::new(((self.bits >> 2) & 0x03) as u8)
    }
    #[doc = "Bits 4:5 - Mass Erase Enable Bits"]
    #[inline(always)]
    pub fn meen(&self) -> MEEN_R {
        MEEN_R::new(((self.bits >> 4) & 0x03) as u8)
    }
    #[doc = "Bits 6:7 - Backdoor Key Security Enable"]
    #[inline(always)]
    pub fn keyen(&self) -> KEYEN_R {
        KEYEN_R::new(((self.bits >> 6) & 0x03) as u8)
    }
}
